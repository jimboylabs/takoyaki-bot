package main

import (
	"context"
	"net/url"
	"os"
	"os/signal"
	"path"
	"strings"

	"github.com/charmbracelet/log"

	"github.com/go-telegram/bot"
	"github.com/go-telegram/bot/models"
	twitterscraper "github.com/n0madic/twitter-scraper"
)

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	opts := []bot.Option{
		bot.WithDefaultHandler(handler),
	}

	b, err := bot.New(os.Getenv("TELEGRAM_BOT_TOKEN"), opts...)
	if err != nil {
		panic(err)
	}

	b.Start(ctx)
}

func handler(ctx context.Context, b *bot.Bot, update *models.Update) {
	scraper := twitterscraper.New()

	// check if text is twitter url

	if strings.HasPrefix(update.Message.Text, "https://twitter.com") {

		u, err := url.Parse(update.Message.Text)
		if err != nil {
			log.Print(err)
			return
		}

		u.RawQuery = ""

		tweetID := path.Base(u.String())
		log.Print("tweet id", tweetID)

		t, err := scraper.GetTweet(tweetID)
		if err != nil {
			panic(err)
		}

		if len(t.Videos) > 0 {
			for _, video := range t.Videos {
				params := &bot.SendVideoParams{
					ChatID:           update.Message.Chat.ID,
					Video:            &models.InputFileString{Data: video.URL},
					ReplyToMessageID: update.Message.ID,
				}

				_, err := b.SendVideo(ctx, params)
				if err != nil {
					log.Print("error", err)
				}
			}
		}

		log.Print("videos", t.Videos)

		if len(t.Photos) > 0 {
			for _, photo := range t.Photos {
				params := &bot.SendPhotoParams{
					ChatID:           update.Message.Chat.ID,
					Photo:            &models.InputFileString{Data: photo.URL},
					ReplyToMessageID: update.Message.ID,
				}

				_, err := b.SendPhoto(ctx, params)
				if err != nil {
					log.Print("error", err)
				}
			}
		}
	}
}
