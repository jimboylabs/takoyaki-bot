FROM golang:1.20.4-alpine as backend

RUN apk update && apk add git

WORKDIR /app

COPY . .

RUN go build -o app

RUN ls

FROM alpine:3.16 AS final

RUN apk add --no-cache ca-certificates tzdata

COPY --from=backend /app/app /app

ENTRYPOINT ["/app"]